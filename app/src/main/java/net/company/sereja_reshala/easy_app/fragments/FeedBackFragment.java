package net.company.sereja_reshala.easy_app.fragments;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import net.company.sereja_reshala.easy_app.ApplicationContext;
import net.company.sereja_reshala.easy_app.R;
import net.company.sereja_reshala.easy_app.databinding.FragmentFeedBackBinding;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class FeedBackFragment extends Fragment {
    FragmentFeedBackBinding mBinding;
    String address = "http://mail-solovev.net/admin/mail_send/send/";
    //    String address = "http://443938.ru/test.php";
    String TAG = "FeedBackFragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_feed_back, container, false);
        mBinding = DataBindingUtil.bind(v);


        mBinding.send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mBinding.name.getText().toString().isEmpty() &&
                        !mBinding.email.getText().toString().isEmpty() &&
                        !mBinding.comment.getText().toString().isEmpty()) {
                    if (isEmailValid(mBinding.email.getText())) {
                        new SendMessage(mBinding.name, mBinding.email, mBinding.comment, mBinding.send)
                                .execute(address, mBinding.name.getText().toString(),
                                        mBinding.email.getText().toString(), mBinding.comment.getText().toString());
                    } else {
                        Toast.makeText(getContext(), "Введите верный email", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getContext(), "Заполните все поля", Toast.LENGTH_LONG).show();
                }
            }
        });


        return v;

    }

    private class SendMessage extends AsyncTask<String, Integer, Integer> {
        EditText name, email, message;
        Button btn;

        public SendMessage(EditText name, EditText email, EditText message, Button btn) {
            this.name = name;
            this.email = email;
            this.message = message;
            this.btn = btn;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (btn != null) {
                btn.setEnabled(false);
            }
        }

        @Override
        protected Integer doInBackground(String... params) {
            return postData(params[0], params[1], params[2], params[3]);
        }

        protected void onPostExecute(Integer result) {
            if (btn != null) {
                btn.setEnabled(true);
            }

            if (result == 200) {
                name.setText("");
                email.setText("");
                message.setText("");
                Toast.makeText(ApplicationContext.getContext(), "Отправлено", Toast.LENGTH_LONG).show();
            } else
                Toast.makeText(ApplicationContext.getContext(), "Не отправлено", Toast.LENGTH_LONG).show();
        }


        public int postData(String address, String name, String email, String message) {
            try {
                List<NameValuePair> nameValuePairs = new ArrayList<>(3);
                DefaultHttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(address);
                nameValuePairs.add(new BasicNameValuePair("name", name));
                nameValuePairs.add(new BasicNameValuePair("email", email));
                nameValuePairs.add(new BasicNameValuePair("message", message));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));
                HttpResponse response = httpclient.execute(httppost);
                return response.getStatusLine().getStatusCode();
            } catch (IOException e) {
                Log.d(TAG, "postData:   " + e);
            }
            return 0;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setupUI(getView());
    }

    boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(getActivity());
                    return false;
                }

            });
        }

        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (activity.getCurrentFocus() != null && activity.getCurrentFocus().getWindowToken() != null)
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }
}
