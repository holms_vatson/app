package net.company.sereja_reshala.easy_app.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.company.sereja_reshala.easy_app.R;
import net.company.sereja_reshala.easy_app.databinding.FragmentVideoBinding;


public class VideoFragment extends Fragment {
    FragmentVideoBinding mBinding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_video, container, false);
        mBinding = DataBindingUtil.bind(v);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        YouTubeFragment f;
        f = YouTubeFragment.newInstance(getResources().getString(R.string.url));
        mBinding.description.setText(getResources().getString(R.string.description));
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.video_container, f).commit();
    }
}
