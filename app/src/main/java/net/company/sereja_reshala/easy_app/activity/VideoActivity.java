package net.company.sereja_reshala.easy_app.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import net.company.sereja_reshala.easy_app.R;
import net.company.sereja_reshala.easy_app.config.VideoData;
import net.company.sereja_reshala.easy_app.databinding.VideoActivityBinding;
import net.company.sereja_reshala.easy_app.fragments.YouTubeFragment;

public class VideoActivity extends AppCompatActivity {
    VideoActivityBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_activity);
        mBinding = DataBindingUtil.setContentView(this, R.layout.video_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_keyboard_backspace_white_36dp);
        mBinding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        VideoData videoData = new VideoData();
        Intent intent = getIntent();
        if (intent != null) {
            videoData.setUrl(intent.getStringExtra("url"));
            videoData.setDescription(intent.getStringExtra("description"));
        }
        if (findViewById(R.id.video_container_v2) != null) {
            YouTubeFragment f;
            f = YouTubeFragment.newInstance(videoData.getUrl());
            mBinding.description.setText(videoData.getDescription());
            getSupportFragmentManager().beginTransaction().replace(R.id.video_container_v2, f).commit();
        }
    }
}
