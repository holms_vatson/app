package net.company.sereja_reshala.easy_app.fragments;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.company.sereja_reshala.easy_app.R;
import net.company.sereja_reshala.easy_app.adapters.PagerAdapter;
import net.company.sereja_reshala.easy_app.databinding.FragmentStartBinding;

import java.util.ArrayList;
import java.util.List;

public class StartFragment extends Fragment {
    String TAG = "StartFragment";
    FragmentStartBinding mBinding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate() returned: ");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_start, container, false);
        mBinding = DataBindingUtil.bind(v);
        mBinding.tabLayout.addTab(mBinding.tabLayout.newTab().setText("начало"));
        mBinding.tabLayout.addTab(mBinding.tabLayout.newTab().setText("доп уроки"));
        mBinding.tabLayout.addTab(mBinding.tabLayout.newTab().setText("связь"));
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(new VideoFragment());
        fragments.add(new ContentFragment());
        fragments.add(new FeedBackFragment());
        final ViewPager viewPager = mBinding.pager;
        final PagerAdapter adapter = new PagerAdapter
                (getChildFragmentManager(), mBinding.tabLayout.getTabCount(), fragments);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mBinding.tabLayout));
        mBinding.tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        return v;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}