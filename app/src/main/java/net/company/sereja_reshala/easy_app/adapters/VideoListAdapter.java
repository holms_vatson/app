package net.company.sereja_reshala.easy_app.adapters;


import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.company.sereja_reshala.easy_app.activity.StartActivity;
import net.company.sereja_reshala.easy_app.activity.VideoActivity;
import net.company.sereja_reshala.easy_app.config.VideoData;
import net.company.sereja_reshala.easy_app.databinding.VideoListBinding;

import java.util.List;

public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.ViewHolder> {
    protected List<VideoData> mData;
    Context context;
    StartActivity activity;

    public class ViewHolder extends RecyclerView.ViewHolder {
        VideoListBinding mBinding;

        public ViewHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }

    }

    public VideoListAdapter(List<VideoData> data, Context context, StartActivity activity) {
        mData = data;
        this.context = context;
        this.activity = activity;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        VideoListBinding binding = VideoListBinding.inflate(inflater, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final VideoData data = mData.get(position);
        holder.mBinding.setData(data);
        int drawable_id = activity.getResources().getIdentifier(data.getDrawable(), "mipmap", activity.getPackageName());
        holder.mBinding.image.setImageResource(drawable_id);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startVideo(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    private void startVideo(VideoData data) {
        Intent myIntent = new Intent(activity, VideoActivity.class);
        myIntent.putExtra("url", data.getUrl());
        myIntent.putExtra("description", data.getDescription());
        activity.startActivity(myIntent);
    }
}