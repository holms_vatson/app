package net.company.sereja_reshala.easy_app.config;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Data;

@Data
public class VideoData implements Serializable {

    public VideoData() {
    }

    public VideoData(String drawable, String name, String description, String url) {
        this.drawable = drawable;
        this.name = name;
        this.description = description;
        this.url = url;
    }

    @SerializedName("image")
    public String drawable;

    @SerializedName("name")
    public String name;

    @SerializedName("description")
    public String description;

    @SerializedName("url")
    public String url;
}
