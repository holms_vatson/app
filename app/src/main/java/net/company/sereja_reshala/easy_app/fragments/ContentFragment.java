package net.company.sereja_reshala.easy_app.fragments;

import android.content.res.AssetManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.company.sereja_reshala.easy_app.R;
import net.company.sereja_reshala.easy_app.activity.StartActivity;
import net.company.sereja_reshala.easy_app.adapters.VideoListAdapter;
import net.company.sereja_reshala.easy_app.config.VideoData;
import net.company.sereja_reshala.easy_app.databinding.FragmentContentBinding;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ContentFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    FragmentContentBinding mBinding;
    List<VideoData> mData;
    VideoListAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_content, container, false);
        mBinding = DataBindingUtil.bind(v);
        final LinearLayoutManager llm = new LinearLayoutManager(getContext());
        mBinding.rv.setLayoutManager(llm);
        try {
            String jsonArray = getJsonString(getContext().getAssets());
            mData = new Gson().fromJson(jsonArray, new TypeToken<ArrayList<VideoData>>() {
            }.getType());
            mAdapter = new VideoListAdapter(mData, getContext(), (StartActivity) getActivity());
            mBinding.rv.setAdapter(mAdapter);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return v;
    }

    @NonNull
    private String getJsonString(AssetManager am) throws IOException {
        InputStream is = am.open("data.json");
        BufferedReader r = new BufferedReader(new InputStreamReader(is));
        StringBuilder total = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            total.append(line);
        }
        return total.toString();
    }


}
