package net.company.sereja_reshala.easy_app.fragments;

import android.os.Bundle;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

import net.company.sereja_reshala.easy_app.config.Config;

public class YouTubeFragment extends YouTubePlayerSupportFragment {

    public YouTubeFragment() {
    }

    public static YouTubeFragment newInstance(String url) {
        YouTubeFragment f = new YouTubeFragment();
        Bundle b = new Bundle();
        b.putString("url", url);
        f.setArguments(b);
        f.init();
        return f;
    }

    private void init() {
        initialize(Config.DEVELOPER_KEY, new YouTubePlayer.OnInitializedListener() {

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider arg0, YouTubeInitializationResult result) {
                if (result.isUserRecoverableError()) {
                    result.getErrorDialog(getActivity(), 1).show();
                } else {
                    Toast.makeText(getContext(),
                            "YouTubePlayer.onInitializationFailure(): " + result.toString(),
                            Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
                if (!wasRestored) {
                    player.cueVideo(getArguments().getString("url"));
                }
            }
        });
    }
}
