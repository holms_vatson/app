package net.company.sereja_reshala.easy_app.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import net.company.sereja_reshala.easy_app.R;
import net.company.sereja_reshala.easy_app.fragments.StartFragment;
import net.company.sereja_reshala.easy_app.service.LaunchService;

public class StartActivity extends AppCompatActivity {
    AlarmManager am;
    private PendingIntent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_activity);

        Intent intent = new Intent(StartActivity.this, LaunchService.class);
        pendingIntent = PendingIntent.getBroadcast(StartActivity.this, 0, intent, 0);
        am = (AlarmManager) getSystemService(ALARM_SERVICE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (findViewById(R.id.container) != null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new StartFragment()).commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (am != null && pendingIntent != null)
            am.cancel(pendingIntent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (am != null)
            am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + AlarmManager.INTERVAL_DAY, AlarmManager.INTERVAL_DAY, pendingIntent);
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Для выхода из приложения нажмите назад еще раз", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

}
