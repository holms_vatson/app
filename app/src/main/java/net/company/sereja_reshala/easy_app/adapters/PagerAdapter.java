package net.company.sereja_reshala.easy_app.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int numTabs;
    List<Fragment> fragments;

    public PagerAdapter(FragmentManager fm, int numTabs, List<Fragment> fragments) {
        super(fm);
        this.numTabs = numTabs;
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return numTabs;
    }
}
